
class Fixnum
  def in_words
    if (self < 10)
      UNDER10[self]
    elsif (self < 20 && self > 9)
      TEENS[self]
    elsif (self < 100)
      word = TENS[(self / 10) *10]
      if (self % 10) != 0
        "#{word} #{(self % 10).in_words}"
      else
        word
      end
    else
      degree = tell_degree
      remainder = self % degree
      degree_phrase = "#{(self / degree).in_words} #{OVER99[degree]}"
      if (self % degree) != 0
        "#{degree_phrase} #{remainder.in_words}"
      else
        degree_phrase
      end
    end
  end


  def tell_degree
    OVER99.keys.take_while { |degree| degree <= self }.last
  end
end


UNDER10 = {
  0 => "zero",
  1 => "one",
  2 => "two",
  3 => "three",
  4 => "four",
  5 => "five",
  6 => "six",
  7 => "seven",
  8 => "eight",
  9 => "nine"
}

TEENS = {
  10 => "ten",
  11 => "eleven",
  12 => "twelve",
  13 => "thirteen",
  14 => "fourteen",
  15 => "fifteen",
  16 => "sixteen",
  17 => "seventeen",
  18 => "eighteen",
  19 => "nineteen"
}

TENS = {
  20 => "twenty",
  30 => "thirty",
  40 => "forty",
  50 => "fifty",
  60 => "sixty",
  70 => "seventy",
  80 => "eighty",
  90 => "ninety"
}

OVER99 = {
  100 => "hundred",
  1000 => "thousand",
  1000000 => "million",
  1000000000 => "billion",
  1000000000000 => "trillion"
}
